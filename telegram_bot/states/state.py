from aiogram.dispatcher.filters.state import StatesGroup, State


class UrlState(StatesGroup):
    url = State()
    step = State()
