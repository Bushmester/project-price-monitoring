from aiogram.types import CallbackQuery

from loader import dp


@dp.callback_query_handler(text_contains='false')
async def finish_work(call: CallbackQuery):
    await call.answer(cache_time=60)
    await call.message.answer('Пока!!!')