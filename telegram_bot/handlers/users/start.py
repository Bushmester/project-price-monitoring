from aiogram import types
from aiogram.dispatcher.filters.builtin import CommandStart

from keyboards.inline.buttons import button

from loader import dp


@dp.message_handler(CommandStart())
async def start(message: types.Message):
    await message.answer(
        text=f'Привет, {message.from_user.full_name}! \n'
        'Я бот ebay парсер! '
        'Я могу сказать тебе, появился ли товар по твоей ссылке по более выгодной цене',
        reply_markup=button
    )
