from scrapy.crawler import CrawlerProcess

from ebay_parsing.ebay_parsing.spiders.ebay_spider import EBaySpider

from database.db_connector import DataBase

from data_exchange_with_db import data_validation_cycle


async def parse(url):
    url_lst = []
    url_lst.append(url)

    process = CrawlerProcess()

    process.crawl(EBaySpider, start_urls=url_lst)
    process.start()

    db = DataBase()
    db.table_create()

    await data_validation_cycle(db, url)
