from aiogram import types
from aiogram.types import CallbackQuery
from aiogram.dispatcher import FSMContext

from scrapy.crawler import CrawlerProcess
from ebay_parsing.ebay_parsing.spiders.ebay_spider import EBaySpider

from database.db_connector import DataBase

from states.state import UrlState

from .functions.validation_check import validation_check

from .parse import parse

from loader import dp, scheduler


@dp.callback_query_handler(text_contains='true')
async def begin_work(call: CallbackQuery):
    await call.answer(cache_time=60)
    await call.message.answer('Отправь ссылку на нужный каталог товаров')

    await UrlState.url.set()


@dp.message_handler(state=UrlState.url)
async def begin_parse(message: types.Message, state: FSMContext):
    user_url = message.text
    await state.update_data(url=user_url)
    data = await state.get_data()
    url = data.get('url')

    if validation_check(url):      
        scheduler.start()
        scheduler.add_job(parse, 'interval', minutes=30, args=[url])
    else:
        await message.answer('Неправильная ссылка')
