from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


button = InlineKeyboardMarkup()

start_work = InlineKeyboardButton(
    text='Нажми, для начала работы', callback_data='start:true'
)
button.add(start_work)

finish_work = InlineKeyboardButton(
    text='Нажми, для завершения работы', callback_data='finish:false'
)
button.add(finish_work)
