import sqlite3
import re

class DataBase:
    def __init__(self):
        self.db = sqlite3.connect('main_data.db')
        self.cursor = self.db.cursor()

    def table_create(self):
        #describes fields in the database and creates them
        self.cursor.execute("""CREATE TABLE IF NOT EXISTS ads (
            title TEXT,
            link_on_product TEXT,
            link_on_category TEXT,
            price TEXT,
            shipping_price TEXT
        )""")

        self.db.commit()

    def json_unpacking(self, json_data:list, link_on_category:str) -> dict:
        #This function unpacks a json file, while finding the product with the lowest price in it
        all_products = []

        for i in range(1, len(json_data)):
            data = {
                "title":json_data[i]["title"],
                "price":json_data[i]["price"],
                "link_on_product":json_data[i]["link"],
                "shipping_price":json_data[i]['shipping_price'],
                "link_on_category":link_on_category
            }
            all_products.append(data)

        the_best_price = ''.join(re.findall(r'[\d,]+', data['price']))
        the_best_price = the_best_price.replace(',','.')
        the_best_price = float(the_best_price)
        the_best_price_product = data

        for product in all_products:
            product['price'] = float(''.join(re.findall(r'[\d,]+', product['price'])).replace(',', '.'))
            if product['price'] < the_best_price:
                the_best_price = product['price'] 
                the_best_price_product = product

        return the_best_price_product

    def data_entry(self, input_data:dict):
    #This function enters data into the database, while checking if this data is unique. (protected from sql injection)
        self.cursor.execute("SELECT link_on_product FROM ads")
        all_links = self.cursor.fetchall()
        if not (input_data['link_on_product'],) in all_links:
            self.cursor.execute(f"INSERT INTO ads VALUES (?, ?, ?, ?, ?)", (
                input_data['title'],
                input_data['link_on_product'],
                input_data['link_on_category'],
                input_data['price'],
                input_data['shipping_price']
                ))
            self.db.commit()

    def records_counter(self) -> int:
        quantity = self.cursor.execute("SELECT COUNT(*) FROM ads")
        return quantity


def price_check(db, input_data:dict) -> dict:
        #This is a check for price changes, it may not return anything if there is no change.
        db.cursor.execute(f"SELECT * FROM ads WHERE link_on_category=?", (input_data['link_on_category'],))
        verifiable_data = db.cursor.fetchone()
        
        if input_data['price'] != float(verifiable_data[3]):
            price_difference = abs(int(verifiable_data[3]) - input_data['price'])
            
            db.cursor.execute(f"UPDATE ads SET title=? price=? shipping_price=? link_on_product=? WHERE link_on_category=?", (input_data['title'], 
                input_data['title'],
                input_data['price'], 
                input_data['shippping_price'], 
                input_data['link_on_product'], 
                input_data['link_on_category']))

            db.db.commit()
            db.cursor.execute(f"SELECT * FROM ads WHERE link_on_category=?", (input_data['link_on_category'],))
            
            changed_records = list(db.cursor.fetchone())
            changed_records = {
                'title': changed_records[0],
                'link_on_product': changed_records[1],
                'price': changed_records[3],
                'shipping_price':changed_records[4],
                'price_difference': price_difference
            }

            return changed_records
