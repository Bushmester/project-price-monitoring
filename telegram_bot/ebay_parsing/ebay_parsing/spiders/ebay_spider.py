import scrapy

from ..items import EbayParsingItem


class EBaySpider(scrapy.Spider):
    name = 'ebay_spider'
    start_urls = []

    custom_settings = {
        'FEED_FORMAT': 'json',
        'FEED_URI': 'ebay.json'
    }

    def parse(self, response):

        items = EbayParsingItem()

        for ebay in response.css('.s-item    '):
            title = ebay.css('.s-item__title')
            title = title.css('::text').get()
            link = ebay.css('.s-item__link::attr(href)').get()
            price = ebay.css('span.s-item__price')
            price = price.css('::text').get()
            shipping_price = ebay.css('.s-item__shipping::text').get()

            items['title'] = title
            items['link'] = link
            items['price'] = price
            items['shipping_price'] = shipping_price

            yield items

        next_page = response.css('.pagination__next::attr(href').get()

        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)
