import os
import json


# This script creates a database, and also runs all checks and necessary actions related to the database.
def data_validation_cycle(db, link_on_category: str) -> dict:
    # (may not return anything)
    with open('ebay.json') as file:
        json_data = json.load(file)
        unpack_data = db.json_unpacking(json_data, link_on_category)
        db.data_entry(unpack_data)
        changed_data = price_check(db, unpack_data)
    os.remove('ebay.json')
    if changed_data:
        return changed_data
