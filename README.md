# Price Monitoring Project

## About
This repository includes a program for monitoring the specified product. 

## Set up
Clone this repository
```
git clone https://gitlab.com/Bushmester/project-price-monitoring.git
```

## Configuration to run
1. Go to the project directory
2. Install virtualenv
```
pip install virtualenv
```
3. Create a virtual environment
```
python3 -m venv price
```
4. Activate the virtual environment
```
source price/bin/activate
```
5. Install the modules
```
pip install -r requirements.txt
```
6. To create in .env file
- BOT_TOKEN (for a bot token from @botfather)